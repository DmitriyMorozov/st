import { writable } from 'svelte/store';

export const form_show = writable(false);
export const dell_show = writable(false);
export const insert_show = writable(false);

export const insertDB_show = writable(false);
export const dellDB_show = writable(false);