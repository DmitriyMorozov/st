import { supabase } from "$lib/supabaseClient";

import { fail, redirect } from '@sveltejs/kit';

export async function load() {
  const { data } = await supabase.from("countries").select()
  .order('id', { ascending: true })
;
  
  return {
    countries: data ?? [],
    
  };
}
/**@type {import('./$types').Actions} */
export const actions = {
  update_country: async ({ locals, cookies, url, request }) => {
      const data = await request.formData()

      let name = data.get("name")
      let id = data.get('id')

      const { error } = await supabase
          .from('countries')
          .update({ name: name })
          .eq('id', id)

      if (error) {
          return fail(parseInt(error.code), { error: error.message });
      }
  },
  del_country: async ({ locals, cookies, url, request }) => {
    const data = await request.formData()

         let id = data.get('id')
         console.log(typeof(id))
         console.log("ghjfdf",id)
        const { error } = await supabase
        .from('countries')
        .delete()
        .eq('id', id)

        console.log(error)

   if (error) {
       return fail(parseInt(error.code), { error: error.message });
    }

},
ins_country: async ({ locals, cookies, url, request }) => {
  const data = await request.formData()

  let name = data.get("name")

      

const { error } = await supabase
  .from('countries')
  .insert([
   
    { 'name': name},
  ])


      console.log(error)

 if (error) {
     return fail(parseInt(error.code), { error: error.message });
  }

}
}
