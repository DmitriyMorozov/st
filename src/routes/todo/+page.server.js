


import { fail, redirect } from '@sveltejs/kit';
import { supabase } from "$lib/supabaseClient";


export async function load({ locals }) {
	if (locals.stop) {
	  throw redirect(303, '/login')
	} else {
		const role  = await supabase.from("user").select().eq("fk_roles" , '1');

		let flag = false;
		for (let i = 0; i < Number(role.data?.length); i++) {
			if (role) {
				if (role.data[i].id == locals.session.data.user.id) {
					flag = true;
					break;
				} else {
					flag = false;
				}
			}
		}
		const { data } = await supabase.from("todo").select().eq("user_id" , locals.session.data.user.id);
		if (flag == true) {	
			const users = await supabase.from("user").select().eq("fk_roles" , '2');
			return {
				todos: data,
				admin : flag,
				users : users.data
				
			};		
		} else {
			return {
				todos: data,
				admin : flag,
				users : []
			}
		};
	}
}

export const actions = {
	create: async ({ cookies, request}) => {
		const data = await request.formData();
		try {
			db.createTodo(cookies.get('user_id'), data.get('description'), data.get('date'));
			return {result : true}
	} catch (error) {
		return fail(422, {
			description: data.get('description'),
			error: error.message
		});
	}
	},
	delete: async ({ cookies, request }) => {
		const data = await request.formData();
		db.deleteTodo(cookies.get('user_id'),
        data.get('id'));
	},
	deletebase: async ({ request }) => {
		const data = await request.formData();

		if(!data.admin) {
			const new_user = await supabase
            .from('todo')
            .delete()
			.eq("id",data.get("id"))

		}
		
	},
	createbase: async ({ request, locals }) => {
		const data = await request.formData();

		// console.log('[createbase]', data.get("offile"));

		const new_todo = await supabase
            .from('todo')
            .insert(
                {
                    value: data.get('description'),
                    user_id: locals.session.data.user.id,
                    date: data.get('date'),
					image: data.get("b64"),
					document: data.get("b642"),
					docName: data.get("offile")?.name
                }
            )
		// console.log('[createbase]',new_todo)
	},	
};
