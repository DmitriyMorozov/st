import { fail, redirect } from '@sveltejs/kit';

import * as fs from 'fs'
import { Buffer } from 'buffer';

import { supabase } from "$lib/supabaseClient";

// import { env } from '$env/dynamic/public' //test with /private!!!

/**@type {import('./$types').PageServerLoad} */
export async function load({ locals }) {
    if (locals.stop == true) {
        throw redirect(303, '/login')
    }

};

/**@type {import('./$types').Actions} */
export const actions = {
    add_image: async ({ locals, cookies, url, request }) => {
        const fdata = await request.formData()

        let b64 = fdata.get("b64")
        let txt = fdata.get('txt')
        let offile = fdata.get("offile")
        let date= fdata.get('date')
        let docName = offile?.valueOf
        // console.log("offile-", offile, offile.name)
        console.log('[OFFILE ADSOMESHIT]', offile ? Object.keys(offile) : 'AAAAAA')

        const { data, error } = await supabase
            .from('todo')
            .insert([
                { value: txt, date:date, image: b64 ,docName: docName},
            ])
            .select()


        if (error) {
            // console.log(error)
            return fail(parseInt(error.code), { error: error.message });
        }

        if (offile) {
            const filedata = new Uint8Array(Buffer.from(await offile.arrayBuffer()));
            fs.writeFile(`static/${offile.name}`, filedata, (err) => {
                if (err) {
                    return fail(500, { error: err.message });
                }
                console.log('Файл сохранен');
            });
        }
        
    },
    del_todo: async ({ locals, cookies, url, request }) => {
        const data = await request.formData()
    
             let id = data.get('id')
             console.log(typeof(id))
             console.log("ghjfdf",id)
            const { error } = await supabase
            .from('todo')
            .delete()
            .eq('id', id)
    
            console.log(error)
    
       if (error) {
           return fail(parseInt(error.code), { error: error.message });
        }
    
    },
}

