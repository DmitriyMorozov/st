
import { SESSIONNAME } from '$env/static/private';
import { json, type RequestHandler, redirect, fail } from '@sveltejs/kit';

export const GET: RequestHandler = async ({ locals, url }) => {

    let id = url.searchParams.get('id')
    let cntr = url.searchParams.get('country')

    const { error } = await locals.sb
        .from('countries')
        .update({ name: cntr })
        .eq('id', id)

    console.log("error", error)
    throw redirect(303, `/supabase`);
};

