import { supabase } from "$lib/supabaseClient";

/**@type {import('./$types').PageServerLoad} */
export async function load({ params }) {
    const { data } = await supabase.from("todo")
        .select()
        .order('id', { ascending: true })

    console.log(data);

    return {
        todo: data ?? [],
    }
};
